﻿using System;
using System.Configuration;
using System.IO;
using CsharpTask.Services;

namespace CsharpTask
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = ConfigurationManager.AppSettings["filePath"];
            Console.WriteLine("Application started\n");
            Console.WriteLine("\n--------------Input the line to search------------");
            string searchLine = Console.ReadLine();
            var SearchObj = new SearchText();
            int SearchResults = SearchObj.FindInFile(filePath, searchLine);
            Console.WriteLine(filePath);

            if (SearchResults != -1)
            {
                Console.WriteLine($"Line found at line no. = {SearchResults}");
            }
            else
            {
                Console.WriteLine("Line not Found");
            }
        }

    }
}
