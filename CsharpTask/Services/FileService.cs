﻿using System.IO;

namespace CsharpTask.Services
{
    class FileService : IFileService
    {
        public StreamReader GetReaderForFile(string filePath)
        {
            StreamReader reader = new StreamReader(filePath);
            return reader;
        }
    }
}
