﻿using System.IO;

namespace CsharpTask.Services
{
    public interface IFileService
    {
        public StreamReader GetReaderForFile(string filePath);
    }
}
