﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CsharpTask.Services
{
    class SearchText
    {
        public int lineNumber { get; private set; }

        public string line { get; set; }

        public int FindInFile(string filePath, string searchLine)
        {
            lineNumber = 1;
            line = ""; 
            FileService fs = new FileService();
            var reader = fs.GetReaderForFile(filePath);

            while ((line = reader.ReadLine()) != null)
            {
                if (line.Contains(searchLine))
                {
                    return lineNumber;
                }
                lineNumber++;
            }
            return -1;
        }
    }
}
